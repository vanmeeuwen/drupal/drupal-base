<?php

namespace Drupal\vm_services\Factory;

use Drupal\Core\Url;

/**
 * Class UrlFactory.
 *
 * @package Drupal\vm_services\Service
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
final class UrlFactory implements UrlFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function fromRoute($routeName, array $routeParameters = [], array $options = []): Url {
    return Url::fromRoute($routeName, $routeParameters, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function fromUri(string $uri, array $options = []): Url {
    return Url::fromUri($uri, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function fromUserInput($url, array $options = []): Url {
    return Url::fromUserInput($url, $options);
  }

}
